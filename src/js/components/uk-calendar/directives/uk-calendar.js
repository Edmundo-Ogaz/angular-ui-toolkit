angular.module('uk-toolkit.components')
    .directive('ukCalendar', function($filter, $locale) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                onChangeDate: '&',
                minDate : '='
            },
            transclude: false,
            templateUrl: 'uk-calendar/uk-calendar.tpl.html',

            link: function(scope, element, attrs, ctrls, ngModel) {
                //Validator Step
                var ctrl = ctrls[0];
                ctrl.$validators.isDate = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };

            scope.$watch('ngModel', function(newValue, oldValue) {
                
                if (newValue) {
                    scope.vm.setDate(newValue);
                }
            }, true);

            },
            controller: function($scope, $element, $q) {
                // ------------------------------
                // Model && default values
                // ------------------------------
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true
                };



                var parseDate = function(chosenDate) {
                    return new Date(chosenDate);
                };

                //static date (always set the last date)

                //Get Tomorrow Date
                var tomorrowDate = new Date();

                var chosenDate = tomorrowDate.toISOString();
         
                if ($scope.minDate) {
                    if (chosenDate < $scope.minDate) {
                        chosenDate = $scope.minDate;
                    }
                }
                chosenDate = parseDate(chosenDate);

                vm.currentDate = new Date(chosenDate);
                var getWeekMonths = function(date) {
                    var month = date.getMonth();
                    var year = date.getFullYear();

                    var from = new Date(year, month, 1);
                    var to = new Date(new Date(year, month + 1, 1) - 1);

                    //We need to start on Monday (1) and end's on Sunday (0)
                    var calendarFrom = from;
                    var calendarTo = to;
                    while (calendarFrom.getDay() !== 1) {
                        calendarFrom.setDate(calendarFrom.getDate() - 1);
                    }
                    while (calendarTo.getDay() !== 0) {
                        calendarTo.setDate(calendarTo.getDate() + 1);
                    }

                    //Now .... we need to split the days in groups of week's (7 days)
                    //so.. start to building the "week's"
                    var weeks = [];
                    var currentWeek = [];
                    var untilTime = calendarTo.getTime();
                    while (calendarFrom.getTime() < untilTime) {
                        var isSelected = false;
                        if (chosenDate.getDate() === calendarFrom.getDate() &&
                            chosenDate.getMonth() === calendarFrom.getMonth() &&
                            chosenDate.getFullYear() === calendarFrom.getFullYear()) {
                            isSelected = true;
                        }
                        var muted = null;
                        if($scope.minDate){
                           
                            var minDDateFormatted = new Date($scope.minDate.toLocaleDateString());
                            var currentDDateFormatted = new Date(calendarFrom.toLocaleDateString());
                            muted = (( minDDateFormatted > currentDDateFormatted )|| ((calendarFrom.getMonth() !== month)) ) ? true : false;

                        }else{
                            muted = (calendarFrom.getMonth() !== month);
                        }
                        currentWeek.push({
                            date: new Date(calendarFrom),
                            muted: muted,
                            selected: isSelected
                        });

                        calendarFrom.setDate(calendarFrom.getDate() + 1);

                        if (calendarFrom.getDay() === 1) {
                            weeks.push(currentWeek);
                            currentWeek = [];
                        }
                    }
                    return weeks;
                };

                // ------------------------------
                // Private Method's
                // ------------------------------
                var writeValue = function(value) {
                    if (value !== null && value !== undefined) {
                        vm.isValidityValid = true;

                        $scope.ngModel = vm.isValidityValid ? value : null;
                    }
                };

                vm.currentMonthWeek = getWeekMonths(vm.currentDate);
                vm.dayNames = $locale.DATETIME_FORMATS.SHORTDAY;
                vm.monthNames = $locale.DATETIME_FORMATS.MONTH;
                vm.years = [];

                var currentYear = chosenDate.getFullYear();
                for (var i = 0; i < 65; i++) {
                    vm.years.push(currentYear - i);
                }
                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    var value = (vm.viewValue || '');
                    return value.toString().length > 0;
                };

                vm.nextMonth = function() {
                    vm.setMonth(vm.currentDate.getMonth() + 1);
                };
                vm.previousMonth = function() {
                    vm.setMonth(vm.currentDate.getMonth() - 1);
                };
                vm.setMonth = function(month) {
                    vm.currentDate.setMonth(month);
                    vm.currentMonthWeek = getWeekMonths(vm.currentDate);
                    vm.pickerYear = vm.currentDate.getFullYear();
                };
                vm.setYear = function(year) {
                    vm.currentDate.setFullYear(year);
                    vm.currentMonthWeek = getWeekMonths(vm.currentDate);
                    vm.pickerYear = vm.currentDate.getFullYear();
                };
                vm.setDate = function(newDate) {
                    var parsedDate = parseDate(newDate);
                    chosenDate = parsedDate;
                    vm.currentDate = parsedDate;
                    vm.currentMonthWeek = getWeekMonths(vm.currentDate);
                    vm.pickerYear = vm.currentDate.getFullYear();
                };
                vm.selectDate = function(date) {

                    writeValue(date);

                    var handler = $scope.onChangeDate();
                    if (handler) {
                        handler(date);
                    }
                };

                // ------------------------------
                // Event's
                // ------------------------------
            }
        };
    });