angular.module('uk-toolkit.components')
    .directive('ukLoading', function($filter, $Localization, $timeout) {
        return {
            restrict: 'E',
            scope: {
                legend: '@'
            },
            transclude: false,
            templateUrl: 'uk-loading/uk-loading.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
            },
            controller: function($scope, $element, $q) {
            }
        };
    });
