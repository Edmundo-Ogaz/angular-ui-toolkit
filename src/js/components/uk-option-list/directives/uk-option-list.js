angular.module('uk-toolkit.components')
    .directive('ukOptionList', function($filter, $Localization) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                errorResource: '@',
                ngChange: '&',
                itemCollection: '='
            },
            transclude: true,
            templateUrl: 'uk-option-list/uk-option-list.tpl.html',
            compile: function() {
                return {             
                    post: function(scope, element, attrs, controller, transcludeFn) {         

                        //Check Items Exists
                        if (!angular.isDefined(attrs.itemCollection)) {
                            throw {
                                error: "ITEM_COLLECTION_NOT_DEFINED_IN_OPTION_LIST"
                            };
                        }

                        //Not render yet from angularJS , so , transform into a Comment Node
                        var templates = element.find("option-template");
                        //templates.html("<!--" + templates.html().trim() + "-->");

                        scope.vm.template = templates.html().trim();
                    }              
                };
            },
            controller: function($scope, $element, $q,$timeout) {
                // ------------------------------
                // Model && default values
                // ------------------------------
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true
                };

                //Check if the resource exists!
                var locale = $Localization.exists($scope.errorResource);
                if (locale) {
                    vm.errorLabel = $Localization.get($scope.errorResource);
                }

                // ------------------------------
                // Private Method's
                // ------------------------------
                // ------------------------------
                // Exposed Method's
                // ------------------------------
                
                vm.optionChange = function(item) {
                    $scope.ngModel = item;

                    if ($scope.ngChange) {
                        //CALL ON-CHANGE BIND
                        var delay = $timeout(function() {
                            $timeout.cancel(delay);
                            $scope.ngChange();
                        }, 0);

                    }
                };
                // ------------------------------
                // Event's
                // ------------------------------


            }
        };
    });
